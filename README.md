<img style="float: left;" src="docs/assets/logo.png" alt="logo" width="300"/>

## <div style="text-align: right"></div>
## <div style="text-align: right"><span style="color:#5dbaebff">Runbook: </span>MkDocs Deployment Playbook</div>


</br>
</br>

MDM MkDocs Ansible Playbook for Deployments

## <span style="color:#5dbaebff">Table of content</span>

- [Initial Setup](#initial-setup)
- [Example](#example)
- [Default Variables](#default-variables)
- [Discovered Tags](#discovered-tags)
- [Dependencies](#dependencies)
- [License](#license)
- [Author](#author)

---
### Install Git and Ansible

#### Ubuntu

```Bash
sudo apt install git ansible
```

If you need a newer version of ansible you will need to add the ansible ppa
```bash
sudo add-apt-repository ppa:ansible/ansible
sudo apt-get update
```

### Setup Git Repo

#### 1. Go to [https://gitlab.com/altispeed/](https://gitlab.com/altispeed/) and Click on New subgroup

<img src=docs/assets/1.png>

2. Select Create group
<img src=docs/assets/2.png>

3. Enter in Client Name in Group Name field
<img src=docs/assets/3.png>

4. Go to [https://gitlab.com/altispeed/internal/ansible/templates/playbook](https://gitlab.com/altispeed/internal/ansible/templates/playbook) and Fork the project

5. Enter project name and select Visibility Level

6. Select the namespace for the group that you just created and then click Fork Project button

## <span style="color:#5dbaebff">Examples</span>


### <span style="color:#5dbaebff">Host File</span>


```ini
site ansible_host=x.x.x.x ansible_user=localadmin


[firewall]
site


[nginx]
site


[mkdocs]
site


[ssl]
site
```


### <span style="color:#5dbaebff">Vars File</span>


**Path:** /path/to/playbook/repo/inventory/host_vars/site/vars.yml


```yaml
# What group of ssh keys to deploy (i.e. prod or dev)
base_server_environment: "prod"


# Admin account username
base_admin: "localadmin"


mkdocs_url: example.com


mkdocs_repository: https://gitlab+deploy-token-3693254:hsb-QcY-9ZGxbCgyvSbs@gitlab.com/altispeed/mdm/sites/docs.minddripone.com.git
mkdocs_branch: main
```


1. Clone Repository


```bash


git clone --recursive git@gitlab.com:altispeed/internal/ansible/templates/playbook.git


cd /path/to/playbook/repo


```


2. Setup Inventory folder and Vars and Hosts files


3. Run Ansible Playbook to Prepare Server


```bash


ansible-playbook -i inventory/hosts setup.yml --tags "setup-ssh,update-base"


```


4. Change ansible user from root to base_admin user


5. Run Ansible Playbook to Install and Setup Base System


```bash


ansible-playbook -i inventory/hosts setup.yml --tags "setup-all" --ask-become-pass


```


6. Run Ansible Playbook to Lockdown ssh and setup and configure firewall


```bash


ansible-playbook -i inventory/hosts setup.yml --tags "lockdown" --ask-become-pass


```



## <span style="color:#5dbaebff">Default Variables</span>

### <span style="color:#5dbaebff">base_additional_ports</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_additional_ports:
  - port: 22
    proto: tcp
```

### <span style="color:#5dbaebff">base_admin</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_admin: localadmin
```

### <span style="color:#5dbaebff">base_admin_password</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_admin_password: changem3
```

### <span style="color:#5dbaebff">base_server_environment</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_server_environment: dev
```

### <span style="color:#5dbaebff">base_timezone</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
base_timezone: Etc/UTC
```

### <span style="color:#5dbaebff">firewall_additional_ports</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
firewall_additional_ports:
  - port: 22
    proto: tcp
```

### <span style="color:#5dbaebff">mkdocs_backup</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_backup: true
```

### <span style="color:#5dbaebff">mkdocs_backup_location</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_backup_location: /opt/backups
```

### <span style="color:#5dbaebff">mkdocs_backup_user</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_backup_user: localadmin
```

### <span style="color:#5dbaebff">mkdocs_branch</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_branch: main
```

### <span style="color:#5dbaebff">mkdocs_day</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_day: '*'
```

### <span style="color:#5dbaebff">mkdocs_extensions</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_extensions:
  - mkdocs-material-extensions==1.0.3
  - mkdocs-git-revision-date-plugin==0.3.1
  - mkdocs-git-revision-date-localized-plugin==0.11.1
```

### <span style="color:#5dbaebff">mkdocs_framework</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_framework: mkdocs-material==8.1.7
```

### <span style="color:#5dbaebff">mkdocs_group</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_group: www-data
```

### <span style="color:#5dbaebff">mkdocs_hour</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_hour: '*'
```

### <span style="color:#5dbaebff">mkdocs_jinja_version</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_jinja_version: 3.0.3
```

### <span style="color:#5dbaebff">mkdocs_minute</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_minute: 0/10
```

### <span style="color:#5dbaebff">mkdocs_month</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_month: '*'
```

### <span style="color:#5dbaebff">mkdocs_repository</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_repository: git@gitlab.com:altispeed/parachute/websites/csg/website.git
```

### <span style="color:#5dbaebff">mkdocs_url</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_url: example.com
```

### <span style="color:#5dbaebff">mkdocs_user</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_user: www-data
```

### <span style="color:#5dbaebff">mkdocs_version</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_version: 1.2.3
```

### <span style="color:#5dbaebff">mkdocs_web</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
mkdocs_web: nginx
```

### <span style="color:#5dbaebff">nginx_application</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_application: docker
```

### <span style="color:#5dbaebff">nginx_container_base_path</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_container_base_path: /containers/{{ nginx_application }}/nginx
```

### <span style="color:#5dbaebff">nginx_container_config_dir_path</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_container_config_dir_path: '{{ nginx_container_base_path }}/conf.d'
```

### <span style="color:#5dbaebff">nginx_container_data_dir_path</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_container_data_dir_path: '{{ nginx_container_base_path }}/data'
```

### <span style="color:#5dbaebff">nginx_container_image</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_container_image: nginx:latest
```

### <span style="color:#5dbaebff">nginx_container_image_force_pull</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_container_image_force_pull: false
```

### <span style="color:#5dbaebff">nginx_container_retries_count</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_container_retries_count: 10
```

### <span style="color:#5dbaebff">nginx_container_retries_delay</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_container_retries_delay: 10
```

### <span style="color:#5dbaebff">nginx_destination</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_destination: example.com
```

### <span style="color:#5dbaebff">nginx_disable_default</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_disable_default: false
```

### <span style="color:#5dbaebff">nginx_group</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_group: www-data
```

### <span style="color:#5dbaebff">nginx_http_port</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_http_port: 80
```

### <span style="color:#5dbaebff">nginx_https_port</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_https_port: 443
```

### <span style="color:#5dbaebff">nginx_installation_type</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_installation_type: local
```

### <span style="color:#5dbaebff">nginx_network</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_network: '{{ nginx_application }}'
```

### <span style="color:#5dbaebff">nginx_server</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_server: nginx
```

### <span style="color:#5dbaebff">nginx_ssl_enabled</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_ssl_enabled: false
```

### <span style="color:#5dbaebff">nginx_systemd_path</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_systemd_path: /etc/systemd/system
```

### <span style="color:#5dbaebff">nginx_type</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_type: site
```

### <span style="color:#5dbaebff">nginx_url</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_url: example.com
```

### <span style="color:#5dbaebff">nginx_user</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
nginx_user: www-data
```

### <span style="color:#5dbaebff">ssl_additional_urls</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_additional_urls: []
```

### <span style="color:#5dbaebff">ssl_cert_type</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_cert_type: selfsigned
```

### <span style="color:#5dbaebff">ssl_country</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_country: US
```

### <span style="color:#5dbaebff">ssl_email</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_email: techies@altispeed.com
```

### <span style="color:#5dbaebff">ssl_http_redirect_block</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_http_redirect_block: '{{ ssl_nginx_defaults.selfsigned_http_block }}'
```

### <span style="color:#5dbaebff">ssl_https_server_block</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_https_server_block: '{{ ssl_nginx_defaults.selfsigned_https_block }}'
```

### <span style="color:#5dbaebff">ssl_organization</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_organization: example
```

### <span style="color:#5dbaebff">ssl_url</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_url: example.com
```

### <span style="color:#5dbaebff">ssl_urls</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_urls: []
```

### <span style="color:#5dbaebff">ssl_web</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_web: nginx
```

### <span style="color:#5dbaebff">ssl_web_root</span>

#### <span style="color:#5dbaebff">Default value</span>

```YAML
ssl_web_root: '{{ ssl_url }}'
```

## <span style="color:#5dbaebff">Discovered Tags</span>

**_always_**

**_install-updates_**

**_lockdown_**

**_rebuild-mkdocs_**

**_secure-mkdocs_**

**_setup-admin_**

**_setup-all_**

**_setup-firewall_**

**_setup-mkdocs_**

**_setup-nginx_**

**_setup-ssh_**

**_setup-ssl_**

**_start_**

**_stop_**

**_update-base_**


## <span style="color:#5dbaebff">Dependencies</span>

None.

## <span style="color:#5dbaebff">License</span>

GPL-2.0-or-later

## <span style="color:#5dbaebff">Author</span>

Dark Decoy
